#!/bin/bash

# -----------------------------------------------------------------------------
# Licensed to org.fatima
# Start Script for fatima process
# @skeleton jin.freestyle@gmail.com
# -----------------------------------------------------------------------------

trap 'kill $PROC_PID' 1 2 3 15

# -----------------------------------------------------------------------------
# preference
# -----------------------------------------------------------------------------
PROCESS="saturn"
FATIMA_SHARE_CONF_DIR=$FATIMA_HOME/conf
FATIMA_SHARE_LIB_DIR=$FATIMA_HOME/lib
FATIMA_SHARE_JAVALIB_DIR=$FATIMA_HOME/javalib
FATIMA_APP_DIR=$FATIMA_HOME/app/$PROCESS
FATIMA_APP_BIN_DIR=$FATIMA_APP_DIR
FATIMA_APP_CONF_DIR=$FATIMA_APP_DIR
FATIMA_APP_JAVALIB_DIR=$FATIMA_APP_DIR/javalib
WORKING_DIR=$FATIMA_APP_DIR/proc
FILE_ENCODING=UTF-8
FATIMA_RESOURCE=ko

useIPv6=false
useProfiler=false

# -----------------------------------------------------------------------------
# launcher, platform
# -----------------------------------------------------------------------------
LAUNCHER="org.fatima.core.process.launch.DefaultApplicationLauncher"
darwin=false
hpux=false
linux=false
sunos=false
case "`uname`" in
Darwin*) darwin=true;;
Linux*) linux=true;;
HP*) hpux=true;;
Sun*) sunos=true;
esac


# -----------------------------------------------------------------------------
# ensure FATIMA Structure
# -----------------------------------------------------------------------------
if test -z "$FATIMA_HOME" ; then
    echo "We need FATIMA_HOME"
    exit
fi

if test ! -e "$FATIMA_SHARE_CONF_DIR" ; then
    echo "We need $FATIMA_HOME/conf"
    exit
fi

if test ! -e "$FATIMA_SHARE_JAVALIB_DIR" ; then
    echo "We need $FATIMA_HOME/javalib"
    exit
fi

if test ! -e "$WORKING_DIR" ; then
    mkdir $WORKING_DIR
fi

# -----------------------------------------------------------------------------
# profiler setting
# -----------------------------------------------------------------------------
if $useProfiler; then
# JPROFILER
PROFILER_AGENT="-agentpath:${HOME}/EXTLIB/jprofiler6/bin/solaris-sparcv9/libjprofilerti.so=port=9991"
# Your Java Kit
#PROFILER_AGENT="-agentpath:${HOME}/EXTLIB/yjp-10.0.3/bin/solaris-sparc-64/libyjpagent.so=port=9890"
else
    PROFILER_AGENT=""
fi

# -----------------------------------------------------------------------------
# java run dependent parameter
# -----------------------------------------------------------------------------
JAVA_CMD=java
#############################
# GENERAL : -XX:+UseParallelGC (-Xmn50m -XX:+UseParallelOldGC)
# G1 : -XX:+UseG1GC -XX:MaxGCPauseMillis=100 (-XX:NewRatio=1)
# CMS : -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled (-Xmn50m)
# DEBUG : (-XX:+PrintGCDetails -verbose:gc)
# DIE_YOUNG : -XX:SurvivorRatio=4 -XX:TargetSurvivorRatio=90 -XX:MaxTenuringThreshold=31
JAVA_OPT_PREFERENCE="-Dpsname=${PROCESS} -Dfatima.home=${FATIMA_HOME} -Dfile.encoding=${FILE_ENCODING} -Dfatima.resource=${FATIMA_RESOURCE}"
JAVA_OPT_GC="-XX:+UseG1GC"
JAVA_OPT_MEMSET="-Xms64m -Xmx64m -Xss512k"
JAVA_OPT_EXT="-XX:+UseNUMA -XX:+UseCompressedOops -XX:+TieredCompilation"
JAVA_OPT_PLATFORM=

if $useIPv6; then
    JAVA_OPT_PLATFORM="-Djava.net.preferIPv6Addresses=true"
fi

if $sunos; then
    JAVA_OPT_PLATFORM="-Djava.nio.channels.spi.SelectorProvider=sun.nio.ch.PollSelectorProvider"
    LD_PRELOAD_64=/usr/sfw/lib/64/libgcc_s.so
    export LD_PRELOAD_64
fi

JAVA_OPT_ALL=" ${JAVA_OPT_PREFERENCE} ${JAVA_OPT_MEMSET} ${JAVA_OPT_GC} ${JAVA_OPT_EXT} ${JAVA_OPT_PLATFORM}"

# -----------------------------------------------------------------------------
# JAVA LIBRARIES
# -----------------------------------------------------------------------------
JAVA_LIB_PATH=.:${FATIMA_SHARE_LIB_DIR}

# -----------------------------------------------------------------------------
# CLASSPATH - share
# -----------------------------------------------------------------------------
CLASSPATH_DIR=(${FATIMA_SHARE_JAVALIB_DIR} ${FATIMA_APP_JAVALIB_DIR})
CLASSPATH=
for javalib_folder in ${CLASSPATH_DIR[*]}
do
	lib_files=`/bin/ls ${javalib_folder}`
	for lib_file in $lib_files ; do
		if [[ "$lib_file" == *.jar ]]; then
			CLASSPATH=${CLASSPATH}:${javalib_folder}/${lib_file}
			continue
		fi
		if [[ "$lib_file" == *.zip ]]; then
			CLASSPATH=${CLASSPATH}:${javalib_folder}/${lib_file}
			continue
		fi
	done
done

export CLASSPATH

# -----------------------------------------------------------------------------
# Launch
# -----------------------------------------------------------------------------
cd ${WORKING_DIR}
${JAVA_CMD} ${JAVA_OPT_ALL} ${PROFILER_AGENT} -Djava.library.path=${JAVA_LIB_PATH} -Dlogback.configurationFile=file:${FATIMA_SHARE_CONF_DIR}/logback.xml ${LAUNCHER} &
PROC_PID=$!
wait


