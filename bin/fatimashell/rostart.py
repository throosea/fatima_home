#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import argparse
import sys

from fatimashell.lib import common
from fatimashell.lib import utils
from fatimashell.lib import exc
from fatimashell.jupiter import auth
from fatimashell.juno import process

class RoStartProcessShell(object):
	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='rostart',
			description='start process',
		)
		common.CommonEnv().build_parser_argument(parser)
		parser.add_argument('-g', '--group',
							default=None,
							help='process group name')
		parser.add_argument('-a', '--all',
							action='store_true',
							help='start all process')
		parser.add_argument('process', nargs='?', help='process name')
		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		common.CommonEnv().validate_parser_argument(args)
		return args

	def main(self, argv):
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)

		if args.all is False and args.group is None and args.process is None:
			raise exc.CommandError("You must provide a process name or group name")

		kwargs = {
			'username': args.fatima_username,
            'password': args.fatima_password,
            'jupiter_uri': args.fatima_jupiter_uri,
            'timezone': args.fatima_timezone,
            'packageDesc': args.package,
            'debug': args.debug,
            'group': args.group,
            'all': args.all,
            'process': args.process,
        }

		try:
			new_token = auth.Communicator().get_token_from_identity_service(**kwargs)
			kwargs.update({'token': new_token})
			endpoint = auth.Communicator().get_endpoint_of_juno(**kwargs)
			kwargs.update({'endpoint': endpoint})
			process.Communicator().start_process(**kwargs)
		except KeyboardInterrupt:
			sys.exit(0)
		except exc.Unauthorized:
			raise exc.CommandError("Invalid FATIMA Identity credentials.")

def main():
	try:
		RoStartProcessShell().main(sys.argv[1:])

	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


