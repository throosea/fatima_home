#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import argparse
import os
import sys
import time
import subprocess
import json

from fatimashell.lib import common

class UpdateRoShell(object):
	debug = False
	fatima_repo_url = ""
	shared_lib_dir = ""
	target_files = set()
	update_files = dict()

	def __init__(self):
		self.shared_lib_dir = os.environ['FATIMA_HOME'] + "/javalib"
		self.fatima_repo_url = os.environ['FATIMA_REPOSITORY_URI']

	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='roupdate',
			description='update fatima base library',
		)
		common.CommonEnv().build_parser_argument(parser)
		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		common.CommonEnv().validate_parser_argument(args)
		return args

	def load_shared_library_files(self):
		if self.debug:
			print "REPO : {}".format(self.fatima_repo_url)

		filenames = os.listdir(self.shared_lib_dir)
		self.target_files.clear()
		for name in filenames:
			test_file = "{}/{}".format(self.shared_lib_dir,name)
			if os.path.isfile(test_file):
				self.target_files.add(name)

	def test_shared_library_files(self):
		if self.debug:
			print "check shared library files "
		else:
			print "check shared library files ",

		sys.stdout.flush()
		self.update_files.clear()
		for name in self.target_files:
			(upgrade, file) = self.version_check_shared_library(name)
			if upgrade and file is not None:
				self.update_files[file] = name

		if len(self.update_files.keys()) == 0 :
			print "every files are fine"
			return

		print "\ntotal {} files is being updated".format(len(self.update_files))
		for file in self.update_files.keys():
			self.download_files(file, self.update_files[file])

	def version_check_shared_library(self, file):
		cmd = "curl -s -X POST -d \""
		cmd += "fileName={}\" ".format(file)
		cmd += "-k {}/sharedlib/file".format(self.fatima_repo_url)
		try:
			if not self.debug:
				print ".",
				sys.stdout.flush()
			else:
				print "file : {}".format(file)
			output = subprocess.check_output(cmd, shell=True)
			body_resp = json.loads(output)
			return body_resp["needUpgrade"], body_resp["upperFileName"]
		except subprocess.CalledProcessError as e0:
			print "\nchecking {} fail. exit code={}".format(file, e0.returncode)
			sys.exit(1)
		except (ValueError, TypeError) as e1 :
			print "\nfail to build response for {} : {}".format(file, e1)
			sys.exit(1)

	def download_files(self, file, oldfile):
		save_file = "{}/{}".format(self.shared_lib_dir,file)
		cmd = "wget --quiet --no-check-certificate --post-data=\""
		cmd += "fileName={}\" ".format(file)
		cmd += "--output-document=\"{}\" ".format(save_file)
		cmd += "{}/sharedlib/download ".format(self.fatima_repo_url)
		#cmd += ">& /dev/null"

		print "try to update : {} => {}".format(oldfile, file),
		try:
			subprocess.check_call(cmd, stderr=None, shell=True)
			os.remove("{}/{}".format(self.shared_lib_dir,oldfile))
			print " ...OK"
		except subprocess.CalledProcessError as e0:
			print "execute download fail. exit code={}".format(e0.returncode)
			sys.exit(1)
		except Exception as e1 :
			print "download fail : {}".format(e1)
			sys.exit(1)

	def main(self, argv):
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)
		self.debug = args.debug

		self.load_shared_library_files()
		if len(self.target_files) == 0:
			print "there are no shared library files. check environment for FATIMA_HOME"
			return

		self.test_shared_library_files()

def main():
	try:
		UpdateRoShell().main(sys.argv[1:])

	except KeyboardInterrupt:
		sys.exit(1)
	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


