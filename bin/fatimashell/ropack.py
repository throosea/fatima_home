#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import argparse
import sys
import logging

from fatimashell.lib import common
from fatimashell.lib import utils
from fatimashell.lib import exc
from fatimashell.jupiter import auth
from fatimashell.jupiter import pack

class RoPackShell(object):
	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='ropack',
			description='display host/package information',
		)
		common.CommonEnv().build_parser_argument(parser)
		parser.add_argument('-g', '--group',
							default=None,
							help='group name')
		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		common.CommonEnv().validate_parser_argument(args)
		return args

	def main(self, argv):
		# Parse args once to find version
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)

		kwargs = {
			'username': args.fatima_username,
            'password': args.fatima_password,
            'jupiter_uri': args.fatima_jupiter_uri,
            'timezone': args.fatima_timezone,
            'group': args.group,
            'debug': args.debug,
        }

		try:
			new_token = auth.Communicator().get_token_from_identity_service(**kwargs)
			kwargs.update({'token': new_token})
			pack.Communicator().print_package_list(**kwargs)
		except KeyboardInterrupt:
			sys.exit(0)
		except exc.Unauthorized:
			raise exc.CommandError("Invalid FATIMA Identity credentials.")

def main():
	try:
		RoPackShell().main(sys.argv[1:])

	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


