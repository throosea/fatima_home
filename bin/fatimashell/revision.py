#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import argparse
import re
import os
import sys
import time
import subprocess
import json
import datetime

from fatimashell.lib import common
from fatimashell.lib import exc

class RevisionApp(object):
	debug = False
	update = False
	config = False
	app_initial_case = False
	app_name = ""
	app_revision_base = ""
	app_revision_full_path = ""
	app_revision_relative_path = ""
	app_previous_revision_path = ""
	fatima_app_dir = ""

	def __init__(self, app_name, debug, update, config):
		self.app_name = app_name
		self.debug = debug
		self.update = update
		self.config = config
		self.fatima_app_dir = os.environ['FATIMA_HOME'] + "/app"
		fatima_revision_dir = self.fatima_app_dir + "/revision"
		self.app_revision_base = fatima_revision_dir + "/" + app_name

	def get_app_revision(self):
		if self.update:
			if self.check_appfolder_exist() is False:
				raise exc.CommandError("{} revision doesn't exist".format(self.app_name))
		else:
			if self.check_appfolder_exist() is True:
				raise exc.CommandError("{} revision exist".format(self.app_name))

		if not self.update:
			if self.debug:
				print "try to create revision app folder {}".format(self.app_revision_base)
			os.mkdir( self.app_revision_base, 0755 );
			self.app_revision_relative_path = "initial_R001"
			self.app_initial_case = True
		else:
			revision_number = self.pickup_next_revision()
			install_date = datetime.datetime.now().strftime('%Y.%m.%d-%H.%M')
			self.app_revision_relative_path = "{}_R{}".format(install_date,revision_number)
		
		self.app_revision_full_path = "{}/{}".format(self.app_revision_base,self.app_revision_relative_path)

		if self.debug:
			print "try to create revision folder {}".format(self.app_revision_full_path)

		os.mkdir( self.app_revision_full_path, 0755 )
		return (self.app_revision_full_path,self.app_revision_relative_path)

	def check_appfolder_exist(self):
		if self.debug:
			print "Check revision : {}".format(self.app_revision_base)
		return os.path.exists(self.app_revision_base)

	def pickup_next_revision(self):
		dirnames = os.listdir(self.app_revision_base)
		numbers = []
		for name in dirnames:
			if os.path.isfile(name):
				continue
			items = name.split("_R")
			item_len = len(items)
			if item_len != 2:
				continue
			numbers.append(int(items[1]))

		numbers.sort()
		last_revision = numbers[len(numbers)-1]
		self.app_previous_revision_path = self.get_previous_revision(dirnames, last_revision)
		return "{:03d}".format(last_revision + 1)

	def get_previous_revision(self, dirs, last_revision):
		find = "R{:03d}".format(last_revision)
		for name in dirs:
			if os.path.isfile(name):
				continue
			if name.endswith(find):
				return name

		return ""

	def remove_revision(self):
		cmd = "\\rm -rf {}".format(self.app_revision_base)
		if self.debug:
			print "remove app revision : {}".format(cmd)
		subprocess.check_call(cmd, stderr=None, shell=True)

	def link_app(self):
		if self.debug:
			print "link: {}".format(self.app_name)

		if not self.app_initial_case:
			cmd = "cd {};unlink {}".format(self.fatima_app_dir, self.app_name)
			subprocess.check_call(cmd, stderr=None, shell=True)
			
		cmd = "cd {};ln -s revision/{}/{} {}".format(self.fatima_app_dir, self.app_name, self.app_revision_relative_path, self.app_name)
		if self.debug:
			print "exec : {}".format(cmd)
		subprocess.check_call(cmd, stderr=None, shell=True)

		if self.config and self.update:
			self.copy_previous_config()

	def copy_previous_config(self):
		self.copy_xml_file(self.app_name + ".xml")
		self.copy_xml_file("spring-" + self.app_name + ".xml")

	def copy_xml_file(self, file_name):
		cmd = "cd {};cp -f revision/{}/{}/{} {}".format(self.fatima_app_dir, self.app_name, self.app_previous_revision_path, file_name, self.app_name)
		subprocess.check_call(cmd, stderr=None, shell=True)

def main():
	try:
		InstallFatimaAppShell().main(sys.argv[1:])

	except KeyboardInterrupt:
		sys.exit(1)
	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


