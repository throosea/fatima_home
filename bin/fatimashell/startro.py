#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import os
import sys
import time

ro_program = ['jupiter', 'juno', 'naptune', 'saturn']

class StartRoShell(object):
	def build_shell_path(self, pgrm):
		path = "{}/app/{}/{}.sh".format(os.environ['FATIMA_HOME'],pgrm,pgrm)
		if not os.path.exists(path):
			return "{}/app/{}/{}".format(os.environ['FATIMA_HOME'],pgrm,pgrm)
		return path

	def start_process(self, pgrm):
		print "check process {}".format(pgrm)
		shell_path = self.build_shell_path(pgrm)
		if not os.path.isfile(shell_path):
			return

		try:
			pid = os.system(shell_path + " &")
			print "process {} START".format(pgrm)	
			time.sleep(1)
		except Exception as e:
			print >> sys.stderr, e

	def main(self, argv):
		for pgrm in ro_program:
			self.start_process(pgrm)

def main():
	try:
		StartRoShell().main(sys.argv[1:])

	except KeyboardInterrupt:
		sys.exit(1)
	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


