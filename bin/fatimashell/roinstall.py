#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import argparse
import re
import os
import sys
import time
import subprocess
import json
import datetime

from revision import RevisionApp
from fatimashell.lib import common
from fatimashell.lib import exc

class InstallFatimaAppShell(object):
	debug = False
	update = False
	config = False
	kwargs = {}
	process_name = ""
	fatima_repo_url = ""

	def __init__(self):
		self.fatima_repo_url = os.environ['FATIMA_REPOSITORY_URI']

	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='roinstall',
			description='install fatima app',
		)
		common.CommonEnv().build_parser_argument(parser)
		parser.add_argument('process', nargs='?', help='process name')
		parser.add_argument('-u', '--update',
							action='store_true',
							help='Update Far')
		parser.add_argument('-c', '--config',
							action='store_true',
							help='Use previous revision config')

		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		common.CommonEnv().validate_parser_argument(args)
		return args

	def install_app(self):
		if self.debug:
			print "REPO : {}".format(self.fatima_repo_url)

		appRevision = RevisionApp(**self.kwargs)
		(revision_full, revision_relative) = appRevision.get_app_revision()
		save_file = "{}/{}.far".format(revision_full,self.process_name)

		if self.debug:
			print "Downloading : {}".format(save_file)

		cmd = "wget --quiet --no-check-certificate --post-data=\""
		cmd += "processName={}\" ".format(self.process_name)
		cmd += "--output-document=\"{}\" ".format(save_file)
		cmd += "{}/app/download ".format(self.fatima_repo_url)

		if self.debug:
			print "command : {}".format(cmd)

		print "try to install {}... ".format(self.process_name)
		try:
			subprocess.check_call(cmd, stderr=None, shell=True)

			# check file size
			if os.path.getsize(save_file) == 0:
				appRevision.remove_revision()
				return

			# jar xvf ...
			if self.debug:
				print "unzip : {}".format(save_file)
			else:
				print "unzip " + self.process_name

			cmd = "cd {}; jar xvf {}.far > /dev/null".format(revision_full, self.process_name)
			subprocess.check_call(cmd, stdout=None, stderr=None, shell=True)

			# remove file..
			if self.debug:
				print "removing : {}".format(save_file)

			os.remove(save_file)

			# process java libs
			JavalibCompare(revision_full).diet()
			
			# change executing shell
			ShellModeChange(self.process_name, revision_full).change()

			# link
			appRevision.link_app()

			print "{} process installed".format(self.process_name)
			if not self.update:
				print "you should edit conf/fatima-package.yaml"

		except subprocess.CalledProcessError as e0:
			print "execute download fail. exit code={}".format(e0.returncode)
			self.remove_app_revision()
			sys.exit(1)
		except Exception as e1 :
			print "download fail : {}".format(e1)
			sys.exit(1)

	def main(self, argv):
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)
		self.debug = args.debug
		self.update = args.update
		self.config = args.config
		self.process_name = args.process

		self.kwargs = {
			'debug': args.debug,
			'update': args.update,
			'config': args.config,
			'app_name': args.process,
		}

		if args.process is None:
			raise exc.CommandError("You must provide a process name")

		self.install_app()

class JavalibCompare(object):
	javalib = ""
	app_javalib = ""
	revision_full_path = ""
	compare_files = set()

	def __init__(self, revision_full):
		self.revision_full_path = revision_full
		self.javalib = os.environ['FATIMA_HOME'] + "/javalib"
		self.app_javalib = revision_full + "/javalib"

	def diet(self):
		print "diet javalib..."
		filenames = os.listdir(self.javalib)
		self.compare_files.clear()
		for name in filenames:
			test_file = "{}/{}".format(self.javalib, name)
			if os.path.isfile(test_file):
				self.compare_files.add(re.split('-[0-9]', name)[0])

		filenames = os.listdir(self.app_javalib)
		for name in filenames:
			test_file = "{}/{}".format(self.app_javalib, name)
			if os.path.isfile(test_file):
				prefix = re.split('-[0-9]', name)[0]
				if prefix in self.compare_files:
					os.remove(test_file)

class ShellModeChange(object):
	shell_file = ""

	def __init__(self, process_name, revision_full):
		self.shell_file = revision_full + "/" + process_name + ".sh"

	def change(self):
		os.chmod(self.shell_file, 0755)

def main():
	try:
		InstallFatimaAppShell().main(sys.argv[1:])

	except KeyboardInterrupt:
		sys.exit(1)
	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


