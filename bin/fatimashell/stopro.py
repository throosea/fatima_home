#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import os
import sys
import signal
import time
import subprocess

ro_program = ['juno', 'jupiter', 'saturn', 'naptune']

class StopRoShell(object):
	pid_map = dict()
	opm_map = dict()

	def get_id(self):
		if sys.platform != 'darwin':
			return subprocess.check_output(["whoami"]).strip()

		id_str1 = subprocess.check_output(["id"]).split("uid=")[1]
		return "\s*{}".format(id_str1.split("(")[0])

	def retrieve_fatima_process(self):
		output = ""
		my_id = self.get_id()
		lcmd = "ps -ef | grep \"^" + my_id + "\" | grep -v grep | grep 'java -Dpsname' | grep fatima.home"
		try:
			output = subprocess.check_output(lcmd, shell=True)
		except Exception as e:
			print "there are no fatima process"
			return

		for line in output.split("\n"):
			if len(line.strip()) < 1:
				continue
			self.add_process_into_map(line)
		print "total {} fatima process found".format(len(self.pid_map))

	def add_process_into_map(self, args):
		pname = ""
		pid = -1
		i = 0
		for token in args.split():
			if len(token.strip()) < 1:
				continue

			i += 1
			if i == 2 :
				pid = int(token)
				continue

			if not token.startswith('-Dpsname=') :
				continue
			#pairs = token.split('=')
			#pname = pairs[1]
			pname = token.split('=')[1]
			self.pid_map[pname] = pid

	def terminate_none_opm_process(self):
		predefined_set = set(ro_program)
		for key in self.pid_map.keys():
			if key in predefined_set :
				self.opm_map[key] = self.pid_map[key]
				continue
			self.terminate_process(key, self.pid_map[key])

	def terminate_opm_process(self):
		for key in ro_program:
			pid = self.read_pid_from_file(key)
			if pid > 0 :
				self.terminate_process(key, pid)
			
	def read_pid_from_file(self, pgrm):
		pid_file = "{}/app/{}/proc/{}.pid".format(os.environ['FATIMA_HOME'],pgrm,pgrm)
		if not os.path.exists(pid_file):
			return 0

		f = open(pid_file, 'r')
		data = f.read()
		return int(data)

	def terminate_process(self, pgrm, pid):
		try:
			print "try to kill {}. pid {}".format(pgrm, pid)
			os.kill(pid, signal.SIGTERM)
			print "process {} was KILLED".format(pgrm)
			time.sleep(1)
		except Exception as e:
			print >> sys.stderr, e

	def main(self, argv):
		self.retrieve_fatima_process()
		self.terminate_none_opm_process()
		self.terminate_opm_process()

		#for pgrm in ro_program:
			#self.shutdown_process(pgrm)

"""
	def read_proc_id(self, pgrm):
		pid_file = "{}/app/{}/proc/{}.pid".format(os.environ['FATIMA_HOME'],pgrm,pgrm)
		if not os.path.isfile(pid_file):
			return -1;
		with open(pid_file, 'r') as pid_log:
			pid = pid_log.read().strip();
			if pid.isdigit():
				return int(pid)
			return None

	def shutdown_process(self, pgrm):
		print "check process {}".format(pgrm)
		pid = self.read_proc_id(pgrm)
		if pid is None or pid < 3:
			return
		terminate_process(pid)
"""
def main():
	try:
		StopRoShell().main(sys.argv[1:])

	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


