#
# org.fatima 
# jin.freestyle@gmail.com
#

from fatimashell.lib import exc
from fatimashell.lib import jsonhttp
from fatimashell.lib import utils
from fatimashell.lib import common

DEPLOY_DIS_RESOURCE_URL = "/schedule/dis/v1"
DEPLOY_UNREGI_RESOURCE_URL = "/schedule/unregi/v1"

class Communicator(jsonhttp.JsonHttp):
    def unregi_schedule(self, packageDesc=None, **kwargs):
        jupiter_uri = kwargs['jupiter_uri']

        try:
            retrieve_url = jupiter_uri + DEPLOY_UNREGI_RESOURCE_URL
            resp, body = self._base_retrieve_package(retrieve_url, 
                                    packageDesc=packageDesc,
                                    **kwargs)
            print "{} ({})".format(
                resp.headers['fatima-response-time'],
                resp.headers['fatima-timezone'])
            print body['system']['message']
        except exc.CommunicationError:
            raise
        except exc.HTTPNotAcceptable:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise

    def dis_schedule(self, packageDesc=None, **kwargs):
        jupiter_uri = kwargs['jupiter_uri']

        try:
            retrieve_url = jupiter_uri + DEPLOY_DIS_RESOURCE_URL
            resp, body = self._base_retrieve_package(retrieve_url, 
                                    packageDesc=packageDesc,
                                    **kwargs)
            print "{} ({})".format(
                resp.headers['fatima-response-time'],
                resp.headers['fatima-timezone'])
            self.print_summary(body['summary'])
        except exc.CommunicationError:
            raise
        except exc.HTTPNotAcceptable:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise

    def _base_retrieve_package(self, retrieve_url, packageDesc=None, **kwargs):
        token = kwargs['token']
        timezone = kwargs['timezone']
        group = kwargs['group']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        params = dict()
        if group is not None:
            params['group'] = group
        if packageDesc is not None:
            params['package'] = packageDesc

        resp, body = self.request(retrieve_url, body=params, headers=headers)
        return resp, body

    def print_summary(self, summary):
        for report in summary:
            self.print_report(report)
            print "\n"

    def print_report(self, r):
        print "[{}] {}:{}".format(r['package_group'],r['package_host'],r['package_name'])
        if 'message' in r:
            print "{}".format(r['message'])
            return 
  
        history = r['deploy_queue']['history']
        schedule = r['deploy_queue']['schedule']

        print "- [scheduled deploy]"
        for s in schedule:
            print "{} ({}) : {}".format(s['deploy_time'],s['timezone'],s['process_name'])

        print "- [deploy history]"
        for h in history:
            print "{}".format(h)

