#
# org.fatima 
# jin.freestyle@gmail.com
#

from fatimashell.lib import exc
from fatimashell.lib import jsonhttp
from fatimashell.lib import utils
from fatimashell.lib import common

PROC_ADD_RESOURCE_URL = "/proc/regist/v1"
PROC_REMOVE_RESOURCE_URL = "/proc/unregist/v1"

class Communicator(jsonhttp.JsonHttp):
    def proc_add(self, packageDesc=None, **kwargs):
        jupiter_uri = kwargs['jupiter_uri']
        retrieve_url = jupiter_uri + PROC_ADD_RESOURCE_URL
        self.do_request(retrieve_url, packageDesc, **kwargs)

    def proc_remove(self, packageDesc=None, **kwargs):
        jupiter_uri = kwargs['jupiter_uri']
        retrieve_url = jupiter_uri + PROC_REMOVE_RESOURCE_URL
        self.do_request(retrieve_url, packageDesc, **kwargs)

    def do_request(self, url, packageDesc, **kwargs):
        try:
            resp, body = self.process_request(url, 
                                    packageDesc,
                                    **kwargs)
            self.print_response(resp, body)
        except exc.CommunicationError:
            raise
        except exc.HTTPNotAcceptable:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise

    def print_response(self, resp, body):
        print "{} ({})".format(
            resp.headers['fatima-response-time'],
            resp.headers['fatima-timezone'])
        print "{}".format(body['system']['message'])

    def process_request(self, retrieve_url, packageDesc=None, **kwargs):
        token = kwargs['token']
        timezone = kwargs['timezone']
        group = kwargs['group']
        process = kwargs['process']
        grp = kwargs['grp']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        params = dict()
        if group is not None:
            params['group'] = group
        if packageDesc is not None:
            params['package'] = packageDesc
        params['process'] = process
        if grp is not None:
            params['group_id'] = grp

        resp, body = self.request(retrieve_url, body=params, headers=headers)
        return resp, body

