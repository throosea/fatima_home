#
# org.fatima 
# jin.freestyle@gmail.com
#

from fatimashell.lib import exc
from fatimashell.lib import jsonhttp
from fatimashell.lib import utils
from fatimashell.lib import common

DEPLOY_RESOURCE_URL = "/deploy/insert/v1"

class Communicator(jsonhttp.JsonHttp):
    def deploy(self, packageDesc=None, **kwargs):
        jupiter_uri = kwargs['jupiter_uri']

        try:
            retrieve_url = jupiter_uri + DEPLOY_RESOURCE_URL
            resp, body = self._base_retrieve_package(retrieve_url, 
                                    packageDesc=packageDesc,
                                    **kwargs)
            print "{} ({})".format(
                resp.headers['fatima-response-time'],
                resp.headers['fatima-timezone'])
            print "{}".format(body['system']['message'])
        except exc.CommunicationError:
            raise
        except exc.HTTPNotAcceptable:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise

    def _base_retrieve_package(self, retrieve_url, packageDesc=None, **kwargs):
        token = kwargs['token']
        timezone = kwargs['timezone']
        group = kwargs['group']
        when = kwargs['when']
        file = kwargs['file']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        params = dict()
        if group is not None:
            params['group'] = group
        if packageDesc is not None:
            params['package'] = packageDesc
        params['when'] = when
        params['file'] = file

        resp, body = self.request(retrieve_url, body=params, headers=headers, file=file)
        return resp, body

