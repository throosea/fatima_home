#
# org.fatima 
# jin.freestyle@gmail.com
#

from fatimashell.lib import exc
from fatimashell.lib import jsonhttp
from fatimashell.lib import utils
from fatimashell.lib import common

PACK_RESOURCE_URL = "/pack/v1"

class Communicator(jsonhttp.JsonHttp):
    def print_package_list(self, **kwargs):
        token = kwargs['token']
        jupiter_uri = kwargs['jupiter_uri']
        timezone = kwargs['timezone']
        group = kwargs['group']

        try:
            retrieve_url = jupiter_uri + PACK_RESOURCE_URL
            resp, body = self._base_retrieve_package(retrieve_url,
                                    token=token,
                                    timezone=timezone,
                                    group=group)
            print "{} ({})".format(
                resp.headers['fatima-response-time'],
                resp.headers['fatima-timezone'])

            fields = ['host','name','endpoint','regist_date','status']
            summary = body['summary']
            deployment = summary['deployment'] 
            for group in deployment:
                print "Group : {}".format(group["group_name"])
                packages = group["deploy"]
                utils.print_list(packages, fields )

            print "Total group:{}, host:{}, package:{}".format(
                summary['group_count'],
                summary['host_count'],
                summary['package_count'])
        except exc.CommunicationError:
            raise
        except exc.HTTPNotAcceptable:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise

    def _base_retrieve_package(self, retrieve_url, token, timezone, group):
        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        params = {}
        if group is not None:
            params = {'group':group}

        url = retrieve_url
        resp, body = self.request(url, body=params, headers=headers)
        return resp, body

