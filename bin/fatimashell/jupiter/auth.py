#
# org.fatima 
# jin.freestyle@gmail.com
#

from fatimashell.lib import exc
from fatimashell.lib import jsonhttp
from fatimashell.lib import utils
from fatimashell.lib import common

LOGIN_RESOURCE_URL = "/auth/login/v1"
PACK_RESOURCE_URL = "/pack/v1"
ENDPOINT_RESOURCE_URL = "/juno/retrieve/v1"

class Communicator(jsonhttp.JsonHttp):
    def get_token_from_identity_service(self, jupiter_uri, username=None,
                                            password=None, 
                                            **kwargs):
        try:
            auth_url = jupiter_uri + LOGIN_RESOURCE_URL
            resp, body = self._base_authN(auth_url,
                                    username=username,
                                    password=password)
            return body['token']
        except exc.CommunicationError:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise exc.AuthorizationFailure("Authorization Failed: "
                                                  "%s" % e)

    def _base_authN(self, auth_url, username=None, password=None):
        headers = {}
        url = auth_url
        if username and password:
            params = {"id": username, "passwd": password}
        else:
            raise ValueError('A username and password or token is required.')
        resp, body = self.request(url, body=params, headers=headers)
        return resp, body

    def get_endpoint_of_juno(self, token, jupiter_uri, packageDesc=None,
                                            **kwargs):
        try:
            retrieve_url = jupiter_uri + ENDPOINT_RESOURCE_URL
            resp, body = self._base_retrieve_juno(retrieve_url,
                                    token=token,
                                    packageDesc=packageDesc)
            return body['endpoint']
        except exc.CommunicationError:
            raise
        except exc.HTTPNotAcceptable:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise

    def _base_retrieve_juno(self, retrieve_url, token, packageDesc=None):
        headers = {"fatima-auth-token": token}
        params = {}
        url = retrieve_url
        if packageDesc:
            params = {"package": packageDesc}

        resp, body = self.request(url, body=params, headers=headers)
        return resp, body

