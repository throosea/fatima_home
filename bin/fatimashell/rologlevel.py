#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import argparse
import sys
import logging

from fatimashell.lib import common
from fatimashell.lib import utils
from fatimashell.lib import exc
from fatimashell.jupiter import auth
from fatimashell.juno import loglevel

class RoLogLevelShell(object):
	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='rolog',
			description='display/change process loglevel',
		)
		common.CommonEnv().build_parser_argument(parser)
		parser.add_argument('process', nargs='?', help='process name or \'all\' for every process')
		parser.add_argument('level', nargs='?', help='log level. e.g) info, debug, ...')
		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		common.CommonEnv().validate_parser_argument(args)
		return args

	def main(self, argv):
		# Parse args once to find version
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)

		if args.process is not None and args.level is None:
			raise exc.CommandError("You must provide complete process name and log level")

		kwargs = {
			'username': args.fatima_username,
            'password': args.fatima_password,
            'jupiter_uri': args.fatima_jupiter_uri,
            'timezone': args.fatima_timezone,
            'packageDesc': args.package,
            'debug': args.debug,
            'process': args.process,
            'level': args.level,
        }

		try:
			new_token = auth.Communicator().get_token_from_identity_service(**kwargs)
			kwargs.update({'token': new_token})
			endpoint = auth.Communicator().get_endpoint_of_juno(**kwargs)
			kwargs.update({'endpoint': endpoint})
			if args.process is not None:
				loglevel.Communicator().change_log_level(**kwargs)
			else:
				loglevel.Communicator().display_log_level(**kwargs)
		except KeyboardInterrupt:
			sys.exit(0)
		except exc.Unauthorized:
			raise exc.CommandError("Invalid FATIMA Identity credentials.")

def main():
	try:
		RoLogLevelShell().main(sys.argv[1:])

	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


