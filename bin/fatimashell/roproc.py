#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import os.path
import argparse
import sys
import logging

from fatimashell.lib import common
from fatimashell.lib import utils
from fatimashell.lib import exc
from fatimashell.jupiter import auth
from fatimashell.jupiter import proc

class RoProcShell(object):
	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='roproc',
			description='add/remove process in package',
		)
		common.CommonEnv().build_parser_argument(parser)
		parser.add_argument('-g', '--group',
							default=None,
							help='group name')
		parser.add_argument('command', 
						help='add or remove')
		parser.add_argument('process', 
						help='process name')
		parser.add_argument('grp', nargs='?', help='process group id') 
		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		common.CommonEnv().validate_parser_argument(args)
		return args

	def main(self, argv):
		# Parse args once to find version
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)

		if args.command == "add":
			if args.grp is None:
				guide = "you have to specify process group id\n"
				guide += "e.g) 2(ENG),3(DB),4(SVC),5(IF)"
				raise exc.CommandError(guide)

		kwargs = {
			'username': args.fatima_username,
            'password': args.fatima_password,
            'jupiter_uri': args.fatima_jupiter_uri,
            'timezone': args.fatima_timezone,
            'group': args.group,
            'packageDesc': args.package,
            'debug': args.debug,
            'command': args.command,
            'process': args.process,
            'grp': args.grp,
        }

		print "finished"

		try:
			new_token = auth.Communicator().get_token_from_identity_service(**kwargs)
			kwargs.update({'token': new_token})
			if args.command == "add":
				proc.Communicator().proc_add(**kwargs)
			if args.command == "remove":
				proc.Communicator().proc_remove(**kwargs)
		except KeyboardInterrupt:
			sys.exit(0)
		except exc.Unauthorized:
			raise exc.CommandError("Invalid FATIMA Identity credentials.")

def main():
	try:
		RoProcShell().main(sys.argv[1:])

	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


