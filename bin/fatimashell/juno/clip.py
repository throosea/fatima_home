#
# org.fatima 
# jin.freestyle@gmail.com
#

import sys
from fatimashell.lib import exc
from fatimashell.lib import jsonhttp
from fatimashell.lib import utils
from fatimashell.lib import common

PACK_RESOURCE_URL = "clip/v1"

class Communicator(jsonhttp.JsonHttp):
    def print_clipboard(self, **kwargs):
        endpoint = kwargs['endpoint']
        token = kwargs['token']
        timezone = kwargs['timezone']

        try:
            retrieve_url = endpoint + PACK_RESOURCE_URL
            resp, body = self._base_retrieve_clipboard(retrieve_url,
                                    token=token,
                                    timezone=timezone)
            print "{} ({})".format(
                resp.headers['fatima-response-time'],
                resp.headers['fatima-timezone'])
            content = body['content']
            print "{}".format(content)

        except exc.CommunicationError:
            raise
        except exc.HTTPNotAcceptable:
            raise
        except (exc.AuthorizationFailure, exc.Unauthorized):
            raise
        except Exception as e:
            raise

    def _base_retrieve_clipboard(self, retrieve_url, token, timezone):
        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        params = {}
        url = retrieve_url
        reload(sys)
        sys.setdefaultencoding('utf-8')
        resp, body = self.request(url, body=params, headers=headers)
        return resp, body

