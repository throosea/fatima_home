#
# org.fatima 
# jin.freestyle@gmail.com
#


from fatimashell.lib import utils
from fatimashell.lib import jsonhttp
from fatimashell.lib import common

RESOURCE_URL = "package/dis/v1"

class Communicator(jsonhttp.JsonHttp):
    def print_process_list(self, **kwargs):
        endpoint = kwargs['endpoint']
        token = kwargs['token']
        timezone = kwargs['timezone']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        endpoint_uri = endpoint + RESOURCE_URL
        params = {}
        resp, body = self.request(endpoint_uri, body=params, headers=headers)
        self.do_process_list(resp.headers, body)
        return resp, body

    def do_process_list(self, headers={}, args={}):
        summary = args['summary']
        fields = ['name', 'pid', 'status', 'cpu', 'mem', 'fd', 'start_time', 'ic', 'group']
        common.print_header(headers, args, summary)
        utils.print_list(args['process_list'], fields )
        ha_status = "UNKNOWN"
        if args['system_status'] == 1:
            ha_status = "ACTIVE"
        elif args['system_status'] == 2:
            ha_status = "STANDBY"
        ps_status = "UNKNOWN"
        if 'system_ps_status' in args:
            if args['system_ps_status'] == 1:
                ps_status = "PRIMARY"
            elif args['system_ps_status'] == 2:
                ps_status = "SECONDARY"

        print "Total:{} (Alive:{},Dead:{}), system is {}/{}".format(summary['total'],
                       summary['alive'],
                       summary['dead'],
                       ha_status,
                       ps_status)

