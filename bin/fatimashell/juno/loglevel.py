#
# org.fatima 
# jin.freestyle@gmail.com
#


from fatimashell.lib import utils
from fatimashell.lib import jsonhttp
from fatimashell.lib import common

DIS_RESOURCE_URL = "loglevel/dis/v1"
CHG_RESOURCE_URL = "loglevel/chg/v1"

class Communicator(jsonhttp.JsonHttp):
    def display_log_level(self, **kwargs):
        endpoint = kwargs['endpoint']
        token = kwargs['token']
        timezone = kwargs['timezone']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        endpoint_uri = endpoint + DIS_RESOURCE_URL
        params = {}
        resp, body = self.request(endpoint_uri, body=params, headers=headers)

        # print result
        summary = body['summary']
        common.print_header(resp.headers, body, summary)

        fields = ['name', 'level']
        utils.print_list(summary['loglevels'], fields)

        return resp, body

    def change_log_level(self, **kwargs):
        endpoint = kwargs['endpoint']
        token = kwargs['token']
        timezone = kwargs['timezone']
        process = kwargs['process']
        level = kwargs['level']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        endpoint_uri = endpoint + CHG_RESOURCE_URL
        params = {"process": process, "loglevel": level}
        resp, body = self.request(endpoint_uri, body=params, headers=headers)

        # print result
        summary = body['summary']
        common.print_header(resp.headers, body, summary)
        print "{}".format(summary['message'])

        return resp, body

