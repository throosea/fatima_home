#
# org.fatima 
# jin.freestyle@gmail.com
#


from fatimashell.lib import utils
from fatimashell.lib import jsonhttp
from fatimashell.lib import common

START_RESOURCE_URL = "process/start/v1"
STOP_RESOURCE_URL = "process/stop/v1"

class Communicator(jsonhttp.JsonHttp):
    def print_summary(self, header, body):
        # print result
        summary = body['summary']
        common.print_header(header,body,summary)
        print "{}".format(summary['message'])
        return

    def start_process(self, **kwargs):
        endpoint = kwargs['endpoint']
        token = kwargs['token']
        timezone = kwargs['timezone']
        all = kwargs['all']
        process = kwargs['process']
        group = kwargs['group']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        endpoint_uri = endpoint + START_RESOURCE_URL
        if all is True:
            params = {"all": "true"}
        else:
            if group is not None: 
                params = {"group": group}
            else:
                params = {"process": process}

        resp, body = self.request(endpoint_uri, body=params, headers=headers)

        self.print_summary(resp.headers, body)
        return resp, body

    def stop_process(self, **kwargs):
        endpoint = kwargs['endpoint']
        token = kwargs['token']
        timezone = kwargs['timezone']
        all = kwargs['all']
        process = kwargs['process']
        group = kwargs['group']

        headers = {"fatima-auth-token": token, "fatima-timezone": timezone}
        endpoint_uri = endpoint + STOP_RESOURCE_URL
        if all is True:
            params = {"all": "true"}
        else:
            if group is not None: 
                params = {"group": group}
            else:
                params = {"process": process}

        resp, body = self.request(endpoint_uri, body=params, headers=headers)

        self.print_summary(resp.headers, body)
        return resp, body

