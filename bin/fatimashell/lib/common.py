#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import argparse
import sys
import logging
import httplib
import requests
import exc

from fatimashell.lib import utils
from fatimashell.lib import jsonhttp

global_print_http=False

class CommonEnv(object):
	def build_parser_argument(self, parser):
		# Global arguments
		parser.add_argument('--fatima-username',
							default=utils.env('FATIMA_USERNAME'),
							help='Defaults to env[FATIMA_USERNAME]')
		parser.add_argument('--fatima-password',
							default=utils.env('FATIMA_PASSWORD'),
							help='Defaults to env[FATIMA_PASSWORD]')
		parser.add_argument('--fatima-jupiter-uri',
							default=utils.env('FATIMA_JUPITER_URI'),
							help='Defaults to env[FATIMA_JUPITER_URI]')
		parser.add_argument('--fatima-timezone',
							default=utils.env('FATIMA_TIMEZONE'),
							help='Defaults to env[FATIMA_TIMEZONE]')
		parser.add_argument('-p', '--package',
							default=None,
							help='Host and Package. e.g) localhost:default')
		parser.add_argument('-d', '--debug',
							action='store_true',
							help='Debug mode')
		return 

	def validate_parser_argument(self, args):
		# a command off the command line
		if not args.fatima_username:
			raise exc.CommandError("You must provide a username via"
				" either --fatima-username or env[FATIMA_USERNAME]")

		if not args.fatima_password:
			raise exc.CommandError("You must provide a password via"
				" either --fatima-password or env[FATIMA_PASSWORD]")

		if not args.fatima_jupiter_uri:
			raise exc.CommandError("You must provide a uri to fatima jupiter via"
				" either --fatima-jupiter-uri or env[FATIMA_JUPITER_URI]")

		self.setup_debugging(args.debug)
		return

	def setup_debugging(self, debug):
		if not debug:
			return

		jsonhttp.debugMode=True

		#streamformat = "%(levelname)s (%(module)s:%(lineno)d) %(message)s"
		#logging.basicConfig(level=logging.DEBUG,
						#format=streamformat)	

def print_header(headers,pack,detail):
	print "{} ({})".format(headers['fatima-response-time'],headers['fatima-timezone'])
	print "[{}] {}:{}".format(
		pack['package_group'],
		pack['package_host'],
		detail['package_name'])

