#
# org.fatima 
# jin.freestyle@gmail.com
#

import os
import sys
import prettytable
import textwrap

def env(*vars, **kwargs):
    """Search for the first defined of possibly many env vars

    Returns the first environment variable defined in vars, or
    returns the default defined in kwargs.
    """
    for v in vars:
        value = os.environ.get(v, None)
        if value:
            return value
    return kwargs.get('default', '')

def print_list(objs, fields, field_labels=None, formatters={}, sortby=0):
    field_labels = field_labels or fields
    pt = prettytable.PrettyTable([f for f in field_labels],
                                 caching=False, print_empty=False)
    pt.align = 'l'

    for o in objs:
        row = []
        for field in fields:
            if field in formatters:
                row.append(formatters[field](o))
            else:
                row.append(o[field])
        pt.add_row(row)
    #print pt.get_string(sortby=field_labels[sortby])
    print pt.get_string()

