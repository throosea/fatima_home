#
# org.fatima 
# jin.freestyle@gmail.com
#

import copy
import requests
import json

from fatimashell.lib import exc

CONTENT_TYPE = 'application/json'
ACCEPT = 'application/json'
USER_AGENT = 'python-fatimaclient'

debugMode = False

class JsonHttp(object):

    def serialize(self, entity):
        return json.dumps(entity)

    def request(self, url, body=None, **kwargs):
        request_kwargs = copy.copy(kwargs)
        request_kwargs.setdefault('headers', kwargs.get('headers', {}))

        if body:
            request_kwargs['data'] = self.serialize(body)

        resp = request(url, **request_kwargs)

        if resp.text:
            try:
                body_resp = json.loads(resp.text)
                if 'system' in body_resp:
                    system_code = body_resp['system']['code']
                    if system_code > 200:
                        raise exc.CommandError(body_resp['system']['message'])

            except (ValueError, TypeError):
                body_resp = None
                print >> stderr, 'Could not decode JSON from body: ' + resp.text
        else:
            print >> stderr, 'No body was returned'
            body_resp = None

        return resp, body_resp

def request(url, file=None, headers=None, data=None, **kwargs):
    if not headers:
        headers = dict()

    headers.setdefault('Accept', ACCEPT)
    headers.setdefault('User-Agent', USER_AGENT)

    method = 'POST'
    files = None
    if file is not None:
        files = {'json': data, 'far': open(file, 'rb')}
        data = None
    else:
        headers.setdefault('Content-Type', CONTENT_TYPE)

    if debugMode:
        print "=== {} {} ============>".format(method,url)
        print "header : {}".format(headers)
        print "body : {}".format(data)
        if file is not None:
            print "file : {}".format(file) 
        print ""

    try:
        resp = requests.request(
            method,
            url,
            headers=headers,
            data=data,
            files=files,
            timeout=(2,2),
            **kwargs)
    except requests.ConnectionError:
        msg = 'Unable to establish connection to %s' % url
        raise exc.CommunicationError(msg)

    if debugMode:
        print "<== {} {} =============".format(resp.status_code,resp.reason)
        print "header : {}".format(resp.headers)
        if resp.text and resp.text.strip():
            print "body : {}".format(json.loads(resp.text))
        print ""

    if resp.status_code == 401:
        raise exc.Unauthorized()
    elif resp.status_code >= 400:
        error_message = None;
        if resp.text and resp.text.strip():
            error_body = json.loads(resp.text)
            if error_body['message']:
                error_message = error_body['message']
        if error_message is None:
            raise exc.HTTPException("error : %d" % resp.status_code)
        else:
            raise exc.HTTPException("error : {} ({})".format(
                resp.status_code, error_message))

    return resp

