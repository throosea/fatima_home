#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import os
import sys
import argparse

from fatimashell.lib import common
from fatimashell.lib import exc

class StatusRoShell(object):
	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='stsro',
			description='show/control package status',
		)
		parser.add_argument('set', nargs='?', help='set package status')
		parser.add_argument('status', nargs='?', help='active/standby')
		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		return args

	def build_shell_path(self, pgrm):
		return "{}/app/{}/{}.sh".format(os.environ['FATIMA_HOME'],pgrm,pgrm)

	def get_package_ha_file(self):
		return "{}/package/cfm/ha/system.ha".format(os.environ['FATIMA_HOME'])

	def is_package_active(self):
		ha_file = self.get_package_ha_file()
		if not os.path.isfile(ha_file):
			raise exc.CommandError("ha file not found")
			return

		with open(ha_file, 'r') as ha_status:
			status = ha_status.read().strip();
			if status == "1":
				return True		
			return False

	def set_package_status(self, status):
		ha_file = open(self.get_package_ha_file(), "w")
		ha_file.write(status)
		ha_file.close()
		if status == "1":
			print "set to ACTIVE"
		else:
			print "set to STANDBY"

	def main(self, argv):
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)

		if args.set is None:
			if self.is_package_active():
				print "ACTIVE"
			else:
				print "STANDBY"
			return

		if args.status is None:
			guide = "you have to specify active or standby"
			raise exc.CommandError(guide)

		new_status = args.status.lower()
		if new_status == "active":
			self.set_package_status("1")
		elif new_status == "standby":
			self.set_package_status("2")
		else:
			print "set should be [active|standby]"
			return

def main():
	try:
		StatusRoShell().main(sys.argv[1:])

	except KeyboardInterrupt:
		sys.exit(1)
	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


