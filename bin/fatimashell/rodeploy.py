#!/usr/bin/python
# org.fatima 
# jin.freestyle@gmail.com
#

"""
Command-line interface to the FATIMA OPM Connect.
"""

import os.path
import argparse
import sys
import logging

from fatimashell.lib import common
from fatimashell.lib import utils
from fatimashell.lib import exc
from fatimashell.jupiter import auth
from fatimashell.jupiter import deploy

class RoDeployShell(object):
	def get_base_parser(self):
		parser = argparse.ArgumentParser(
			prog='rodeploy',
			description='deploy package to server',
		)
		common.CommonEnv().build_parser_argument(parser)
		parser.add_argument('-g', '--group',
							default=None,
							help='group name')
		parser.add_argument('when', 
						help='deploy schedule. \'now\' or \'yyyy-MM-dd-HH-mm\' format')
		parser.add_argument('file', 
						help='upload \'far\' fatima package file')
		return parser

	def process_args_parse(self, parser):
		args = parser.parse_args()
		common.CommonEnv().validate_parser_argument(args)
		return args

	def main(self, argv):
		# Parse args once to find version
		parser = self.get_base_parser()
		args = self.process_args_parse(parser)

		if not os.path.isfile(args.file): 
			raise exc.CommandError("no such file : " + args.file)

		kwargs = {
			'username': args.fatima_username,
            'password': args.fatima_password,
            'jupiter_uri': args.fatima_jupiter_uri,
            'timezone': args.fatima_timezone,
            'group': args.group,
            'packageDesc': args.package,
            'debug': args.debug,
            'when': args.when,
            'file': args.file,
        }

		try:
			new_token = auth.Communicator().get_token_from_identity_service(**kwargs)
			kwargs.update({'token': new_token})
			deploy.Communicator().deploy(**kwargs)
		except KeyboardInterrupt:
			sys.exit(0)
		except exc.Unauthorized:
			raise exc.CommandError("Invalid FATIMA Identity credentials.")

def main():
	try:
		RoDeployShell().main(sys.argv[1:])

	except Exception as e:
		print >> sys.stderr, e
		sys.exit(1)


if __name__ == "__main__":
	main()


